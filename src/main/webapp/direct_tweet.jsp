<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.google.appengine.api.datastore.DatastoreService" %>
<%@ page import="com.google.appengine.api.datastore.DatastoreServiceFactory" %>
<%@ page import="com.google.appengine.api.datastore.Entity" %>
<%@ page import="com.google.appengine.api.datastore.Key" %>
<%@ page import="com.google.appengine.api.datastore.KeyFactory" %>
<%@ page import="com.google.appengine.api.datastore.Query.Filter" %>
<%@ page import="com.google.appengine.api.datastore.Query.FilterOperator" %>
<%@ page import="com.google.appengine.api.datastore.Query.FilterPredicate" %>
<%@ page import="com.google.appengine.api.datastore.Query" %>
<%@ page import="com.google.appengine.api.datastore.PreparedQuery" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="/css/tweet.css">
</head>
<body>
<!DOCTYPE html>
<html>
<head>
<title>Facebook Login JavaScript Example</title>
<meta charset="UTF-8">
</head>
<body>
<div id="shareBtn" class="btn btn-success clearfix">Share Dialog</div>

<script>
document.getElementById('shareBtn').onclick = function() {
  FB.ui({
    display: 'popup',
    method: 'share',
    href: 'https://developers.facebook.com/docs/',
  }, function(response){});
}
</script>
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else {
      // The person is not logged into your app or we are unable to tell.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '230925767664647',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });
    function onLogin(response) {
        if (response.status == 'connected') {
               FB.api('/me?fields=first_name', function(data) {
                      var welcomeBlock = document.getElementById('fb-welcome');
                      welcomeBlock.innerHTML = 'Hello, ' + data.first_name + '!';
                });
        }
        else {
                var welcomeBlock = document.getElementById('fb-welcome');
               welcomeBlock.innerHTML = 'Cant get data ' + response.status + '!';}
 }

 FB.getLoginStatus(function(response) {
      // Check login status on load, and if the user is
      // already logged in, go directly to the welcome message.
      if (response.status == 'connected') {
               onLogin(response);
      } else {
           // Otherwise, show Login dialog first.
           FB.login(function(response) {   onLogin(response);} , {scope: 'user_friends, email'}   );
      }
 });
    // Now that we've initialized the JavaScript SDK, we call 
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me?fields=first_name,id,last_name,email', function(response) {
    	var welcomeBlock = document.getElementById('fb-welcome');
    	var welcomeBlock1 = document.getElementById('fb-welcome1');
    	var welcomeBlock2 = document.getElementById('fb-welcome2');
        welcomeBlock.innerHTML = 'Hello, ' + response.last_name + '!';
        welcomeBlock1.innerHTML = 'Your facebook id is ' + response.id + '!';
        welcomeBlock2.innerHTML = 'Email id is  ' + response.email + '!';
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.first_name + '!';
    });
  }
</script>

<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

<fb:login-button scope="public_profile,email,user_birthday" onlogin="checkLoginState();">
</fb:login-button>

<div class="topnav">

  <a href="tweet.jsp">Tweet</a>
  <a href="friendstweet.jsp">Friends</a>
  <a  id=toptweet>Top Tweet</a>
  <a href="#about"></a>
  <div id="fb-root"></div>
  <div align="right">
  <div class="fb-login-button" data-max-rows="1"    data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="true"  data-use-continue-as="true" scope="public_profile,email" onlogin="checkLoginState();"></div>
  </div>
  </div>
<%
try{
	DatastoreService ds=DatastoreServiceFactory.getDatastoreService();

Entity e=new Entity("tweet");

//Key k=KeyFactory.createKey("tweet", request.getParameter("id"));
//Key k=KeyFactory.stringToKey(request.getParameter("id"));
//out.println(k);
//out.println(temp.getId());
//Filter filter = new FilterPredicate("ID/Name",FilterOperator.EQUAL,request.getParameter("id"));
//out.println(filter.toString());
Query q=new Query("tweet");
//out.println(q.toString());
PreparedQuery pq = ds.prepare(q);
long visited_count=0;
for (Entity result : pq.asIterable()) {
   	  String first_name = (String) result.getProperty("first_name");
	  String lastName = (String) result.getProperty("last_name");
	  String picture = (String) result.getProperty("picture");
	  String status = (String) result.getProperty("status");
	  Long id = (Long) result.getKey().getId();
	  String time = (String) result.getProperty("timestamp");
	  visited_count = (Long)((result.getProperty("visited_count")));
	  Key k= result.getKey();
	  if(id==Long.parseLong(request.getParameter("id"))){
	 // out.println(result.getKey().getId()+" "+first_name + " " + lastName + ", " + picture + " "+visited_count);
	  Entity s=ds.get(KeyFactory.createKey("tweet", id));
	  s.setProperty("visited_count", visited_count+1);
	//  out.println("check"+s.getProperty("visited_count"));
	  ds.put(s);
	  out.println("<h1>Status of "+ s.getProperty("first_name")+" "+s.getProperty("last_name")+"</h1>");
	  out.println("<table frame=box>");
	  out.println("<tr><td><div style="+"height: 50px; width:50px>"+picture+"</div><td>");
	  out.println("<td><div id=name>"+ first_name+" "+lastName +"</div></td>");
	  out.println("<tr><h3><div id=status> "+s.getProperty("first_name")+" said "+status +"</div></h3></tr>");
	  out.println("<tr><div id=postedate>Posted at:"+ time +"</div></tr>");
	  out.println("</table>");
	  }
	}
}catch(Exception e){
	out.println(e);
}
%>
</body>
<script type="text/javascript">
console.log("validate");
</script>
</html>